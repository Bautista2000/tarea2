import os

# Constantes del juego

# Constantes relacionadas con la pantalla y la velocidad de los fotogramas

ANCHO = 640
ALTO = 550
FPS = 60
TITULO = "Mi primer juego con OOP en PyGame"

# Ruta de las imagenes del juego

ESCENARIO_FONDO = os.getcwd() + "/juego_aventuras/elementos/imagenes/Fondo_juego_aventuras.jpg"
PERSONAJE_IMAGEN = os.getcwd() + "/juego_aventuras/elementos/imagenes/Nutty.png"
PLATAFORMA_IMAGEN = os.getcwd() + "/juego_aventuras/elementos/imagenes/Plataforma.jpg"
PISO_IMAGEN = os.getcwd() + "/juego_aventuras/elementos/imagenes/Piso.jpg"
DULCE_IMAGEN = os.getcwd() + "/juego_aventuras/elementos/imagenes/Dulce.png"

GRAVEDAD_JUGADOR = 0.8
ACELERACION_JUGADOR = 0.5
FRICCION_JUGADOR = -0.12

# Colores

NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
VERDE = (0, 255, 0)
ROJO = (255, 0, 0)
AZUL = (0, 0, 255)
VIOLETA = (98, 0, 255)
