import pygame
from juego_aventuras.constantes import *

class Dulce(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load(DULCE_IMAGEN)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y