import pygame
from juego_aventuras.constantes import *
from juego_aventuras.elementos.protagonista import *
from juego_aventuras.elementos.plataforma import *
from juego_aventuras.elementos.piso import *
from juego_aventuras.dulce import *

class Juego():
    def __init__(self):
        self.game_over = False
        self.ganar = False
        self.escenario_imagen = pygame.image.load(ESCENARIO_FONDO)
        self.plataformas = pygame.sprite.Group()
        self.protagonista = Protagonista()
        self.dulces = pygame.sprite.Group()

        p1 = Plataforma(50, 50)
        self.plataformas.add(p1)
        p2 = Plataforma(250, 250)
        self.plataformas.add(p2)
        p3 = Plataforma(500, 350)
        self.plataformas.add(p3)
        d1 = Dulce(110,10)
        d2 = Dulce(310,210)
        d3 = Dulce(560,310)
        self.dulces.add(d1)
        self.dulces.add(d2)
        self.dulces.add(d3)

        piso = Piso(0, ALTO - 70)
        self.plataformas.add(piso)

    def procesar_eventos(self):
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                return True
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if self.game_over or self.ganar:
                    self.__init__()
            if evento.type == pygame.KEYDOWN:
                if evento.key == pygame.K_UP:
                    self.protagonista.saltar()
                    sonido = pygame.mixer.Sound("saltar.ogg")
                    sonido.play()
        return False

    def logica_de_ejecucion(self):
        if not self.game_over:
            self.protagonista.mover()

            # Revisar si el personaje choca con una plataforma
            if self.protagonista.velocidad.y > 0:
                colisiones = pygame.sprite.spritecollide(self.protagonista, self.plataformas, False)
                if colisiones:
                    self.protagonista.posicion.y = colisiones[0].rect.top
                    self.protagonista.velocidad.y = 0
                colision_dulce = pygame.sprite.spritecollide(self.protagonista, self.dulces, True)
                if colision_dulce:
                    if len(self.dulces)==0:
                        self.ganar = True
                        sonido = pygame.mixer.Sound("ganar.ogg")
                        sonido.play()

    def actualizar_pantalla(self, pantalla):
        """ Actualiza la pantalla. """
        if self.game_over:
            fuente = pygame.font.SysFont("comicsansms", 25)
            texto = fuente.render("Game Over, haz click para volver a jugar", True, NEGRO)
            centrar_x = (ANCHO // 2) - (texto.get_width() // 2)
            centrar_y = (ALTO // 2) - (texto.get_height() // 2)
            pantalla.blit(texto, [centrar_x, centrar_y])

        if not self.game_over:
            pantalla.blit(self.escenario_imagen, (0, 0))
            pantalla.blit(self.protagonista.image, self.protagonista.rect)
            self.plataformas.draw(pantalla)
            self.dulces.draw(pantalla)
            fuente = pygame.font.SysFont("comicsansms",20)
            texto = fuente.render("Ayuda a Nutty a estar con sus amados dulces",True,NEGRO)
            pantalla.blit(texto, [220,0])

        if self.ganar:
            fuente = pygame.font.SysFont("comicsansms", 25)
            texto = fuente.render("Haz ganado!!. Haz click para volver a jugar", True, NEGRO)
            centrar_x = (ANCHO // 2) - (texto.get_width() // 2)
            centrar_y = (ALTO // 2) - (texto.get_height() // 2)
            pantalla.blit(texto, [centrar_x, centrar_y])



        pygame.display.flip()
