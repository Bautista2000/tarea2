import pygame
from juego_aventuras.constantes import *


class Protagonista(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(PERSONAJE_IMAGEN)
        self.rect = self.image.get_rect()
        self.rect.center = (ANCHO // 2, ALTO // 2)
        self.posicion = pygame.math.Vector2(ANCHO // 2, ALTO)
        self.velocidad = pygame.math.Vector2(0, 0)
        self.aceleracion = pygame.math.Vector2(0, 0)

    def saltar(self):
        self.velocidad.y = -20

    def mover(self):
        self.aceleracion = pygame.math.Vector2(0, GRAVEDAD_JUGADOR)

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.aceleracion = self.aceleracion + pygame.math.Vector2(-ACELERACION_JUGADOR, 0)
        if keys[pygame.K_RIGHT]:
            self.aceleracion = self.aceleracion + pygame.math.Vector2(ACELERACION_JUGADOR, 0)

        # Aplicar friccion
        self.aceleracion.x += self.velocidad.x * FRICCION_JUGADOR
        # Ecuaciones de movimiento
        self.velocidad =  self.velocidad + self.aceleracion
        self.posicion = self.posicion + (self.velocidad + 0.5 * self.aceleracion)

        if self.posicion.x > ANCHO:
            self.posicion.x = 0
        if self.posicion.x < 0:
            self.posicion.x = ANCHO

        self.rect.midbottom = self.posicion
